//WAP to find the sum of n complex numbers using structures and 4 or more functions.
#include<stdio.h>
int *input(int n)
{
    static int a[20]; 
    int i;
    printf("Enter the integers:\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    return a;
}
int compute(int *a, int n)
{
    int sum=0;
    int i;
    for(i=0;i<n;i++)
    {
        sum=sum+a[i];
    }
    return sum;
}
void output(int sum, int n)
{
    printf("The sum of the %d integers that were entered, is %d.\n",n,sum);
}
int main()
{
    int *a, n, sum; 
    printf("How many numbers do you want to add?\n");
    scanf("%d",&n);
    a=input(n);
    sum=compute(a,n);
    output(sum,n);
    return 0;
}