//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>
float input()
{
    float x;
    scanf("%f",&x);
    return x;
}

float compute_volume(float h, float d, float b)
{
    float vol;
    vol=((h*d*b)+(d/b))/3;
    return vol;
}

void output(float volume)
{
    printf("Volume of tromboloid is: %f\n",volume);
}
float main()
{
    float h,d,b,vol;
    printf("Enter the value of height:\n");
    h=input();
    printf("Enter the value of depth:\n");
    d=input();
    printf("Enter the value of breadth:\n");
    b=input();
    vol=compute_volume(h,d,b);
    output(vol);
    return 0;
}