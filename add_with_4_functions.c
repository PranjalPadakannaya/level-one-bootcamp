//Write a program to add two user input numbers using 4 functions.
#include <stdio.h>
int input1()
{
    int a;
    printf("Enter the first number:\n");
    scanf("%d",&a);
    return a;
}

int input2()
{
    int b;
    printf("Enter the second number:\n");
    scanf("%d",&b);
    return b;
}

int sum(int a, int b)
{
    int sum;
    sum=a+b;
    return sum;
}

int main()
{
    int a, b, c;
    a=input1();
    b=input2();
    c=sum(a,b);
    printf("Sum of the two numbers is %d\n",c);
    return 0;
}