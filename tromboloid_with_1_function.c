//Write a program to find the volume of a tromboloid using one function
#include<stdio.h>
int main()
{
    float h,d,b,vol;
    printf("Enter height, depth and breadth of tromboloid, respectively\n");
    scanf("%f %f %f",&h,&d,&b);
    vol=0.33*((h*d*b)+(d/b));
    printf("Volume of the tromboloid is: %f\n",vol);
    return 0;
}