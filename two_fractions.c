//WAP to find the sum of two fractions.
#include<stdio.h>
struct fraction
{
    int num;
    int den;
};

typedef struct fraction frac;

frac input()
{
    frac f;
    printf("Enter the numerator:\n");
    scanf("%d",&f.num);
    printf("Enter the denominator:\n");
    scanf("%d",&f.den);
    return f;
}

int compute_hcf(int a, int b)
{
	int hcf;
	hcf=1;
	for(int i=2;i<=a&&i<=b;i++)
	{
		if(a%i==0&&b%i==0)
		{
			hcf=i;
		}
	}
	return hcf;
}
frac compute(frac f1, frac f2)
{
    int hcf, i, numer, denom;
    numer=(f1.num*f2.den)+(f1.den*f2.num);
    denom=f1.den*f2.den;
    hcf=compute_hcf(numer,denom);
    frac sum;
    sum.num=numer/hcf;
    sum.den=denom/hcf;
    return sum;
}
void output(frac sum)
{
    printf("The added fraction is %d/%d",sum.num,sum.den);
}
int main()
{
    frac f1,f2,sum;
    f1=input();   
    f2=input();
    sum=compute(f1,f2);
    output(sum);
    return 0;
}
