//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>
float input(float a)
{
    scanf("%f",&a);
    return a;
}
float compute(float x1,float x2,float y1,float y2)
{
    float d;
    d=sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
    return d;
}
void output(float d)
{
    printf("The distance between the two given points are %f units\n",d);
}
int main()
{
    float x1,x2,y1,y2,dist;
    printf("Enter x1 and y1:\n");
    x1=input(x1);
    y1=input(y1);
    printf("Enter x2 and y2:\n");
    x2=input(x2);
    y2=input(y2);
    dist=compute(x1,x2,y1,y2);
    output(dist);
    return 0;
}